mod fizzbuzz_test;

pub fn fizzbuzz(number: i32) -> String {
    let est_multiple_de_3 = number % 3 == 0;
    let est_multiple_de_5 = number % 5 == 0;
    let number_string = number.to_string();
    let contient_un_3 = number_string.contains("3");
    let contient_un_5 = number_string.contains("5");

    match (est_multiple_de_3, est_multiple_de_5, contient_un_3, contient_un_5) {
        (true, true, _, _) => String::from("FizzBuzz"),
        (true, _, false, false) => String::from("Fizz"),
        (_, _, true, false) => String::from("Fizz"),
        (_, true, false, false) => String::from("Buzz"),
        (_, _, false, true) => String::from("Buzz"),
        (_, _, _, _) => number_string
    }
}
