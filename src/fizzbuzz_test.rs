#[cfg(test)]
mod tests {
    use crate::fizzbuzz;

    #[test]
    fn fizzbuzz_call_with_1_return_1() {
        assert_eq!(fizzbuzz(1), "1");
    }

    #[test]
    fn fizzbuzz_call_with_2_return_2() {
        assert_eq!(fizzbuzz(2), "2");
    }

    #[test]
    fn fizzbuzz_call_with_3_return_Fizz() {
        assert_eq!(fizzbuzz(3), "Fizz");
    }

    #[test]
    fn fizzbuzz_call_with_4_return_4() {
        assert_eq!(fizzbuzz(4), "4");
    }

    #[test]
    fn fizzbuzz_call_with_5_return_Buzz() {
        assert_eq!(fizzbuzz(5), "Buzz");
    }

    #[test]
    fn fizzbuzz_call_with_6_return_Fizz() {
        assert_eq!(fizzbuzz(6), "Fizz");
    }

    #[test]
    fn fizzbuzz_call_with_13_return_Fizz() {
        assert_eq!(fizzbuzz(13), "Fizz");
    }

    #[test]
    fn fizzbuzz_call_with_15_return_FizzBuzz() {
        assert_eq!(fizzbuzz(15), "FizzBuzz");
    }

    #[test]
    fn fizzbuzz_call_with_51_return_Buzz() {
        assert_eq!(fizzbuzz(51), "Buzz");
    }
}
